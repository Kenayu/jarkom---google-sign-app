//function for google sign-in button
function onSignIn(googleUser)
{
//    get user's profile data
    var profile=googleUser.getBasicProfile(); 
//    show n hide the sign-in button
    $(".g-signin2").css("display","none");
//   loading user's profile
    $(".data").css("display","block");
//    get user's avatar
    $("#pic").attr('src',profile.getImageUrl());
//    get user's name
    $("#name").text(profile.getName());
//    get user's email
    $("#email").text(profile.getEmail());
}

//function for google sign-out button
function signOut()
{
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
        alert("You have been successfully Signed-Out");
        
        $(".g-signin2").css("display","block");
        $(".data").css("display","none");
    });
}