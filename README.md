# JarKom - Google Sign App

Name    : Niken Ayu Anggarini.
NRP     : 4210181003

Hello! This is a project of google sign-in and sign-out app.


To run this code on my localhost, I need to:
1. Make a folder named "SignApp" in D:\XAMPP\htdocs
2. Copy-Paste the index.html and script.js to SignApp folder
3. Run the code in browser by typing "http://localhost/SignApp/index.html"
4. Try the Sign-In and Sign-Out buttons to check whether they work or not
